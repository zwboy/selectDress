//
//  SelectViewController.h
//  selectDress
//
//  Created by 周伟 on 16/4/7.
//  Copyright © 2016年 zw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry.h>
@interface SelectViewController : UIViewController

@property (nonatomic,strong) UIButton *cameraBtn;

@property (nonatomic,strong) UIButton *albumBtn;

@property (nonatomic,strong) NSDictionary *textDict;

@end
