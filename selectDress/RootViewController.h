//
//  ViewController.h
//  selectDress
//
//  Created by kousen on 16/4/6.
//  Copyright (c) 2016年 zw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry.h>
#import "LocationViewController.h"
@interface RootViewController : UIViewController

@property (nonatomic,strong) UIButton *startBtn;

@property (nonatomic,strong) NSDictionary *textDict;

@end

