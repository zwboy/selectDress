//
//  DressTableViewCell.h
//  selectDress
//
//  Created by 周伟 on 16/4/7.
//  Copyright © 2016年 zw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DressTableViewCell : UITableViewCell

@property (nonatomic,strong) UILabel *tagLab;

@property (nonatomic,strong) UIImageView *firstImageView;

@property (nonatomic,strong) UIImageView *secondImageView;

@property (nonatomic,strong) UIImageView *thirdImageView;

@end
