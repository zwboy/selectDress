//
//  ViewController.m
//  selectDress
//
//  Created by kousen on 16/4/6.
//  Copyright (c) 2016年 zw. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"主页";
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"Text List" ofType:@"plist"];
    self.textDict = [[NSDictionary alloc]initWithContentsOfFile:plistPath];
    self.startBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.startBtn addTarget:self action:@selector(enterToSelectViewController) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.startBtn];
    [self.startBtn setTitle:[self.textDict objectForKey:@"StartBtnTitle"] forState:UIControlStateNormal];
    [self.startBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(self.view.frame.size.width*.5,self.view.frame.size.width*.5));
    }];
}

- (void)enterToSelectViewController{
    LocationViewController *locationVC = [[LocationViewController alloc]init];
    [self.navigationController pushViewController:locationVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
