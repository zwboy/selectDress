//
//  SelectViewController.m
//  selectDress
//
//  Created by 周伟 on 16/4/7.
//  Copyright © 2016年 zw. All rights reserved.
//

#import "SelectViewController.h"

@interface SelectViewController ()

@end

@implementation SelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"Text List" ofType:@"plist"];
    self.textDict = [[NSDictionary alloc]initWithContentsOfFile:plistPath];
    [self addBtns];
    // Do any additional setup after loading the view.
}

- (void)addBtns {
    self.cameraBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.cameraBtn setTitle:[self.textDict objectForKey:@"CameraBtnText"] forState:UIControlStateNormal];
    [self.cameraBtn setBackgroundColor:[UIColor blueColor]];
    self.cameraBtn.layer.cornerRadius = 40;
    [self.view addSubview:self.cameraBtn];
    [self.cameraBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.bottom.equalTo(self.view.mas_centerY).with.offset(-50);
        make.size.mas_equalTo(CGSizeMake(80, 80));
    }];
    
    self.albumBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.albumBtn setTitle:[self.textDict objectForKey:@"AlbumBtnText"] forState:UIControlStateNormal];
    [self.albumBtn setBackgroundColor:[UIColor blueColor]];
    self.albumBtn.layer.cornerRadius = 40;
    [self.view addSubview:self.albumBtn];
    [self.albumBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.equalTo(self.view.mas_centerY).with.offset(50);
        make.size.mas_equalTo(CGSizeMake(80, 80));
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
