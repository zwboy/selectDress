//
//  AppDelegate.h
//  selectDress
//
//  Created by kousen on 16/4/6.
//  Copyright (c) 2016年 zw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

